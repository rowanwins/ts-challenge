export const SOIL_DATA_URL = "https://www.asris.csiro.au/arcgis/rest/services/TERN/DES_ACLEP_AU_TRN_N/MapServer/identify"

export const SOIL_DATA_PARAMS = new URLSearchParams({
  geometry: JSON.stringify({
      "x": 145,
      "y": -35,
      "spatialReference": {
          "wkid": 4326
      }
  }),
  geometryType: "esriGeometryPoint",
  sr: "4326",
  layers: "visible:0",
  tolerance: "2",
  mapExtent: "-180,-90,180,90",
  imageDisplay:" 100,100,96",
  returnGeometry: "false",
  f: "json"
})

export const RAINFALL_DATA_URL = "https://data-cbr.csiro.au/thredds/wms/catch_all/LW_LANDSCAPE/monthly_rainfall/stacked_mean_data/bom-rain_monthly_stacked.nc"

export const RAINFALL_DATA_PARAMS = new URLSearchParams({
  exceptions: "XML",
  version: "1.3.0",
  feature_count: "101",
  time: "2020-01-01T00:00:00.000Z",
  styles: "",
  service: "WMS",
  request: "GetFeatureInfo",
  layers: "rain_month",
  bbox: "15028131.257091936,-3757032.814272985,16280475.528516259,-2504688.542848654",
  width: "256",
  height: "256",
  crs: "EPSG:3857",
  query_layers: "rain_month",
  info_format: "text/xml",
  i: "106",
  j: "197"
})
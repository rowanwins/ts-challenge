## TS Challenge
This little challenge is designed to test your familiarity with some of the Javascript/Typescript ecosystem and watch how you work to break down the problem.
Feel free to use Google to check things if you're uncertain. 
Please add types where you can.

### The Goal
A user is creating a new property in the Ruminati application. When this happens we have the opportunity to retrieve some contextual information about their property based on their location which may influence their emissions and sequestration potential.

Your job is to complete the `getContextualPropertyData` function in `src/index.ts`. This function should make two `fetch` requests to `GET` some useful information for a rural property.
One request will get rainfall information, which if sucessfully executed, will return XML data.
The other request will get soil information, which if successfully executed, will return JSON data.

These two requests should be made concurrently, rather than sequentially.

The `getContextualPropertyData` function should 
- block the remainder of the script from executing
- return an object 
````
{
  rainfall: An XML string (unparsed) or undefined,
  soil: A JSON object or undefined
}
````

The `src/references.ts` provides the URLs and Query String / Search Parameters required to successfully make the requests.